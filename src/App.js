import React, { useState, useEffect } from "react";
import "./App.css";

const io = require("socket.io-client");
const socket = io("http://localhost:3001");

function App() {
  const name = useFromInput("");
  const message = useFromInput("");
  const messages = useMessages([]);

  return (
    <div>
      <header className="App-header">
        <input placeholder="Name" {...name} />
        <input placeholder="Message" {...message} />
        <button
          onClick={() => {
            socket.emit("message-send", {
              name: name.value,
              message: message.value
            });
          }}
        >
          Send
        </button>
        <lu>
          {messages.map(message => (
            <li>
              <span>
                <strong>{message.name}</strong>: {message.message}
              </span>
            </li>
          ))}
        </lu>
      </header>
    </div>
  );
}

export default App;

const useFromInput = initValue => {
  const [value, setValue] = useState(initValue);
  const handleChange = e => {
    setValue(e.target.value);
  };
  return {
    value,
    onChange: handleChange
  };
};

const useMessages = initValue => {
  const [value, setValue] = useState(initValue);
  useEffect(() => {
    socket.on("receive-message", data => {
      setValue(data);
    });
  });
  return value;
};
