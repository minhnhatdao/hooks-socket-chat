const port = 3001;
const express = require("express")();
const server = require("http").createServer(express);
const io = require("socket.io")(server);

let listMessage = [];

io.on("connection", socket => {
  socket.on("message-send", data => {
    listMessage = [...listMessage, data];
    io.sockets.emit("receive-message", listMessage);
  });
});

server.listen(port);
